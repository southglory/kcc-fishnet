﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using KinematicCharacterController;
using KinematicCharacterController.Examples;
using FishNet.Object;
using FishNet;
using FishNet.Object.Prediction;
using Cinemachine;

namespace KinematicCharacterController.Examples
{

    public struct ReconcileData
    {
        public Vector3 Position;
        public Quaternion Rotation;
        public Vector3 BaseVelocity;

        public bool MustUnground;
        public float MustUngroundTime;
        public bool LastMovementIterationFoundAnyGround;
        public CharacterTransientGroundingReport GroundingStatus;
        public Vector3 AttachedRigidbodyVelocity;
    }

    public class ExamplePlayer : NetworkBehaviour
    {
        public ExampleCharacterController Character;
        public ExampleCharacterCamera CharacterCamera;
        public bool UsingNewInputSystem;
        public bool UsingCinemacine;
        public Transform RootMesh;

        //Quang: Old input system member
        private const string MouseXInput = "Mouse X";
        private const string MouseYInput = "Mouse Y";
        private const string MouseScrollInput = "Mouse ScrollWheel";
        private const string HorizontalInput = "Horizontal";
        private const string VerticalInput = "Vertical";

        //Quang: New input system member
        private InputManager localInput;

        //Quang: Cinemacine parameter
        public GameObject CinemachineCameraTarget;
        public float TopClamp = 70.0f;
        public float BottomClamp = -30.0f;
        public float CameraAngleOverride = 0.0f;
        public bool LockCameraPosition = false;

        private GameObject mainCamera;
        private float _cinemachineTargetYaw;
        private float _cinemachineTargetPitch;
        private float _sensitivity = 1f;
        private const float _threshold = 0.01f;

        private void Awake()
        {
            //Quang: Using manual simultion instead of KCC auto simulation
            KinematicCharacterSystem.Settings.AutoSimulation = false;

            //Quang: Subcribe tick event, this will replace FixedUpdate
            InstanceFinder.TimeManager.OnTick += TimeManager_OnTick;

            //Quang: Cache the camera 
            mainCamera = GameObject.FindGameObjectWithTag("MainCamera");

            //Quang: Cache InputManager
            localInput = FindObjectOfType<InputManager>();
        }

        private void OnDestroy()
        {
            if (InstanceFinder.TimeManager != null)
            {
                InstanceFinder.TimeManager.OnTick -= TimeManager_OnTick;
            }
        }

        public override void OnStartClient()
        {
            base.OnStartClient();

            Cursor.lockState = CursorLockMode.Locked;

            if(!UsingCinemacine)
            {
                CharacterCamera.SetFollowTransform(Character.CameraFollowPoint);
                CharacterCamera.IgnoredColliders.Clear();
                CharacterCamera.IgnoredColliders.AddRange(Character.GetComponentsInChildren<Collider>());
            }
            else
            {
                if (!base.IsOwner) return;

                CharacterCamera.gameObject.SetActive(false);
                GameObject.FindGameObjectWithTag("PlayerFollowCamera").GetComponent<CinemachineVirtualCamera>().Follow = CinemachineCameraTarget.transform;
            }
            

            if (!base.IsOwner)
            {
                //Quang: Disable camera on player, instead of destroy in order for implementing inspecting other players
                CharacterCamera.gameObject.SetActive(false);

                //Quang: The remote object must not have movement related logic code, destroy it. Network transform will handle the movements
                if (!base.IsServer)
                {
                    Destroy(GetComponent<KinematicCharacterMotor>());
                    Destroy(GetComponent<ExampleCharacterController>());
                    GetComponent<Rigidbody>().isKinematic = true;
                }
            }
        }

        private void TimeManager_OnTick()
        {
            if (base.IsOwner)
            {
                Reconciliation(default, false);
                HandleCharacterInput(out PlayerCharacterInputs inputData);
                Move(inputData, false);
            }
            if (base.IsServer)
            {
                Move(default, true);
                KinematicCharacterMotorState state = Character.Motor.GetState();
                Reconciliation(translateReconcileData(state), true);
            }
            if (!base.IsOwner && !base.IsServer)
                GetComponent<Rigidbody>().isKinematic= true;
        }

        [Replicate]
        private void Move(PlayerCharacterInputs input, bool asServer, bool replaying = false)
        {
            Character.SetInputs(ref input);

            //Quang: When Fishnet reconcile, it will run this Replicate method as replay in order for redo missing input,
            // so we have to simulate KCC tick manually here, but only when replaying
            if(replaying)
            {
                KinematicCharacterSystem.SimulateThisTick((float)base.TimeManager.TickDelta);
            }
        }

        [Reconcile]
        private void Reconciliation(ReconcileData rd, bool asServer)
        {
            //Quang: Note - KCCMotorState has Rigidbody field, this component is not serialized, 
            // and doesn't have to be reconciled, so we build a new Reconcile data that exclude Rigidbody field
            Character.Motor.ApplyState(translateStateData(rd));
        }
        private ReconcileData translateReconcileData(KinematicCharacterMotorState state)
        {
            ReconcileData rd = new ReconcileData();
            rd.Position = state.Position;
            rd.Rotation = state.Rotation;
            rd.BaseVelocity = state.BaseVelocity;

            rd.MustUnground = state.MustUnground;
            rd.MustUngroundTime = state.MustUngroundTime;
            rd.LastMovementIterationFoundAnyGround = state.LastMovementIterationFoundAnyGround;
            rd.GroundingStatus = state.GroundingStatus;
            rd.AttachedRigidbodyVelocity = state.AttachedRigidbodyVelocity;

            return rd;
        }

        private KinematicCharacterMotorState translateStateData(ReconcileData rd)
        {
            KinematicCharacterMotorState state = new KinematicCharacterMotorState();
            state.Position = rd.Position;
            state.Rotation = rd.Rotation;
            state.BaseVelocity = rd.BaseVelocity;

            state.MustUnground = rd.MustUnground;
            state.MustUngroundTime = rd.MustUngroundTime;
            state.LastMovementIterationFoundAnyGround = rd.LastMovementIterationFoundAnyGround;
            state.GroundingStatus = rd.GroundingStatus;
            state.AttachedRigidbodyVelocity = rd.AttachedRigidbodyVelocity;

            return state;
        }

        private void LateUpdate()
        {
            if(UsingCinemacine)
            {
                HandleCinemacineCameraInput();
            }
            else
            {
                // Handle rotating the camera along with physics movers
                if (CharacterCamera.RotateWithPhysicsMover && Character.Motor.AttachedRigidbody != null)
                {
                    CharacterCamera.PlanarDirection = Character.Motor.AttachedRigidbody.GetComponent<PhysicsMover>().RotationDeltaFromInterpolation * CharacterCamera.PlanarDirection;
                    CharacterCamera.PlanarDirection = Vector3.ProjectOnPlane(CharacterCamera.PlanarDirection, Character.Motor.CharacterUp).normalized;
                }

                HandleCameraInput();
            }
        }

        private void HandleCameraInput()
        {
            if (!base.IsOwner) return;
            // Create the look input vector for the camera
            float mouseLookAxisUp = Input.GetAxisRaw(MouseYInput);
            float mouseLookAxisRight = Input.GetAxisRaw(MouseXInput);
            Vector3 lookInputVector = new Vector3(mouseLookAxisRight, mouseLookAxisUp, 0f);

            // Prevent moving the camera while the cursor isn't locked
            if (Cursor.lockState != CursorLockMode.Locked)
            {
                lookInputVector = Vector3.zero;
            }

            // Input for zooming the camera (disabled in WebGL because it can cause problems)
            float scrollInput = -Input.GetAxis(MouseScrollInput);
#if UNITY_WEBGL
        scrollInput = 0f;
#endif

            // Apply inputs to the camera
            CharacterCamera.UpdateWithInput(Time.deltaTime, scrollInput, lookInputVector);

            // Handle toggling zoom level
            if (Input.GetMouseButtonDown(1))
            {
                CharacterCamera.TargetDistance = (CharacterCamera.TargetDistance == 0f) ? CharacterCamera.DefaultDistance : 0f;
            }

            if (CharacterCamera.TargetDistance == 0f) 
                SetOrientationMethod(OrientationMethod.TowardsCamera);
            else
                SetOrientationMethod(OrientationMethod.TowardsMovement);
        }
        
        //Quang: if using cinemachine
        private void HandleCinemacineCameraInput()
        {
            if (localInput.look.sqrMagnitude >= _threshold && !LockCameraPosition)
            {
                _cinemachineTargetYaw += localInput.look.x;
                _cinemachineTargetPitch += localInput.look.y;
            }

            _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
            _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

            CinemachineCameraTarget.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch + CameraAngleOverride,
                _cinemachineTargetYaw, 0.0f);
        }
        private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
        {
            if (lfAngle < -360f) lfAngle += 360f;
            if (lfAngle > 360f) lfAngle -= 360f;
            return Mathf.Clamp(lfAngle, lfMin, lfMax);
        }

        [ServerRpc(RunLocally = true)]
        private void SetOrientationMethod(OrientationMethod method)
        {
            Character.OrientationMethod = method;
        }

        private void HandleCharacterInput(out PlayerCharacterInputs characterInputs)
        {
            if(!UsingNewInputSystem)
            {
                // Build the CharacterInputs struct
                characterInputs.MoveAxisForward = Input.GetAxisRaw(VerticalInput);
                characterInputs.MoveAxisRight = Input.GetAxisRaw(HorizontalInput);
                characterInputs.CameraRotation = CharacterCamera.Transform.rotation;

                //Quang: Should add jump queued function in order for not missing input, here I use get key for quick demo, should not use in final project
                characterInputs.JumpDown = Input.GetKey(KeyCode.Space);

                characterInputs.CrouchDown = Input.GetKeyDown(KeyCode.C);
                characterInputs.CrouchUp = Input.GetKeyUp(KeyCode.C);
            }
            else
            {
                characterInputs.MoveAxisForward = localInput.move.y;
                characterInputs.MoveAxisRight = localInput.move.x;
                characterInputs.CameraRotation = mainCamera.transform.rotation;
                characterInputs.JumpDown = localInput.jump;
                //Quang: Crouch not implemented XD
                characterInputs.CrouchDown = false;
                characterInputs.CrouchUp = false;

                //Quang: reset jump state
                localInput.jump = false;
            }
        }
    }
}